<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOTPSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otp_settings', function (Blueprint $table) {
            $table->id();
            $table->enum('setting', [
                'length', 
                'allowed_per_hour', 
                'expiration', 
                'resend_grace_period', 
                'resend_max', 
                'allowed_attempts'
            ]);
            $table->integer('value');
            $table->unique(['setting', 'value']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otp_settings');
    }
}
