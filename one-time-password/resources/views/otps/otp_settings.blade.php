@extends('layouts.app')

@section('content')
<div id="otps" class="container">
    @if($errors->any())
        <div class="bg-danger p-2">
            <h4>{{$errors->first()}}</h4>
        </div>
    @endif
    <update-otp-settings><update-otp-settings>
</div>
@endSection
