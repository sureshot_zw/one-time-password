<?php

namespace App\Http\Services;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\OTP;
use App\OTPSettings;
use Exception;
use App\Mail\SendOtpMail;
use Illuminate\Support\Facades\Mail;
use DB;

class OTPService 
{
    /**
     * Digits to use in otp generation
     * 
     * @var string
     */
    const OTP_DIGITS = '0123456789';

    /**
     * Length of the generated OTP
     *
     * @var int
     */
    protected $length;

    /**
     * Otp Validity time
     *
     * @var int
     */
    protected $validity;

    /**
     * maximum number of times a OTP can be resent
     * within resend grace period
     * 
     * @var int
     */
    protected $resendMax;

    /**
     * Maximum otps allowed to generate per hour
     *
     *  @var int
     */
    protected $allowedPerHour;

    /**
     * Grace period in which OTP can be resent
     * 
     * @var int
     */
    protected $resendGracePeriod;

    /**
     * Default constructor
     * 
     */
    public function __construct()
    {
        $this->setOtpDefaults();
    }
    
    /**
     * Set OTP defaults from db or fall over to default config
     * 
     * @return void
     */
    protected function setOtpDefaults()
    {
        $otpDefaults = OTPSettings::all();

        if($otpDefaults->isEmpty()) {
            $this->setDefaultsFromConfig();

            return;
        }

        $otpDefaults->each(function($otpSetting) {
            if($otpSetting->setting === 'length') {
                $this->length = $otpSetting->value;

                return;
            }

            if($otpSetting->setting === 'allowed_per_hour') {
                $this->allowedPerHour = $otpSetting->value;

                return;
            }

            if($otpSetting->setting === 'expiration') {
                $this->validity = $otpSetting->value;

                return;
            }

            if($otpSetting->setting === 'resend_grace_period') {
                $this->resendGracePeriod = $otpSetting->value;

                return;
            }

            if($otpSetting->setting === 'resend_max') {
                $this->resendMax = $otpSetting->value;

                return;
            }

            if($otpSetting->setting === 'allowed_attempts') {
                $this->allowedAttempts = $otpSetting->value;

                return;
            }   
        });
    }

    /**
     * Resend latest OTP
     * 
     * @param string $email
     * 
     * @return object
     */
    public function resendOtp(string $email): object
    {
        $otp = OTP::where('email', $email)->latest()->first();
    
        if(empty($otp) || !$otp->canBeResent()) return $this->generate($email);

        if ($otp->resend_count === $this->resendMax) {
            return (object) [
                'status' => false,
                'message' => "Reached the maximum allowed resends",
            ];
        }

        $otp->expired = false;
        
        if($otp->save()) $otp->increment('resend_count');

        Mail::to($email)->queue(new SendOtpMail($otp));

        return (object) [
            'status' => true,
            'token' => $otp->token,
            'message' => "OTP resent",
        ];
    }

    /**
     * Set defaults from Config file
     * 
     * @return void
     */
    protected function setDefaultsFromConfig()
    {
        $this->length = config('otp-config.defaults.length');
        $this->validity = config('otp-config.defaults.expiration');
        $this->allowedPerHour = config('otp-config.defaults.allowed_per_hour');
        $this->resendGracePeriod = config('otp-config.defaults.resend_grace_period');
        $this->resendMax = config('otp-config.defaults.resend_max');
        $this->allowedAttempts = config('otp-config.defaults.allowed_attempts');
    }

    /**
     * Return OTP settings
     * 
     * @return array
     */
    public function getSettings(): array
    {
        return [
            'length' => $this->length, 
            'allowed_per_hour' => $this->allowedPerHour, 
            'expiration' => $this->validity, 
            'resend_grace_period' => $this->resendGracePeriod, 
            'resend_max' => $this->resendMax,
            'allowed_attempts' => $this->allowedAttempts,
        ];
    }

    /**
     * Generate OTP
     * 
     * @param string $email
     * 
     * @return object
     */
    public function generate(string $email): object
    {
        if($this->hasReachedHourlyRequestThreshold($email)) {
            return (object) [
                'status' => false,
                'message' => "You have requested too many OTP's, please wait an hour and try again",
            ];
        }

        try {
            DB::beginTransaction();
            $otp = OTP::create([
                'email' => $email,
                'token' => $this->createOTPToken(),
                'validity' => $this->validity,
                'generated_at' => Carbon::now(),
            ]);
            
            $otp->increment('generated_count');
    
            Mail::to($email)->queue(new SendOtpMail($otp));
            DB::commit();
    
            return (object) [
                'status' => true,
                'token' => $otp->token,
                'message' => "OTP generated",
            ];
        }catch(Exception $ex) {
            DB::rollBack();
            return (object) [
                'status' => false,
                'message' => "An error occured while trying to generate an OTP, pleaes try again",
            ];
        }
    }

    /**
     * OTP generated in the last 24 hour period or has been used already
     * 
     * @param string $otp
     * 
     * @return bool
     */
    protected function otpUsedAlreadyOrGeneratedInLastDay(string $otp)
    {
        return OTP::where('token', $otp)
                ->orWhere(function($q) use($otp) {
                    $q->whereBetween('created_at', [
                        Carbon::now()->subHours(24)->toDateTimeString(), 
                        Carbon::now()->toDateTimeString()
                    ])->where('token', $otp);
                })->first();
    }

    /**
     * Has user reached their token threshold for a given period
     * 
     * @return bool
     */
    protected function hasReachedHourlyRequestThreshold(string $email): bool
    {
        return OTP::whereBetween('created_at', [
                Carbon::now()->subHours(1)->toDateTimeString(), 
                Carbon::now()->toDateTimeString()
            ])->where('email', $email)
            ->get()
            ->count() >= $this->allowedPerHour;
    }

    /**
     * Validate OTP 
     * 
     * @param string $token
     * @param string $email
     * 
     * @return object
     */
    public function validate(string $token, string $email): object
    {

        $otp = OTP::where('email', $email)->latest()->first();

        if (! $otp) {
            return (object) [
                'status' => false,
                'message' => 'OTP does not exists, Please generate new OTP',
            ];
        }

        if ($otp->isExpired()) {
            return (object) [
                'status' => false,
                'message' => 'OTP has expired',
            ];
        }

        if ($otp->attempted_count == $this->allowedAttempts) {
            return (object) [
                'status' => false,
                'message' => "You have reached the maximum allowed attempts",
            ];
        }

        $otp->increment('attempted_count');
        
        if ($otp->token === $token) {
            $otp->expired = true;
            $otp->save();
            
            return (object) [
                'status' => true,
                'message' => 'OTP is valid',
            ];
        }


        return (object) [
            'status' => false,
            'message' => 'OTP does not match',
        ];
    }

    /**
     * Token Expired at
     * 
     * @return object
     */
    public function expiredAt(string $identifier): object
    {
        $otp = OTP::where('identifier', $identifier)->first();

        if (! $otp) {
            return (object) [
                'status' => false,
                'message' => 'OTP does not exists, Please generate new OTP',
            ];
        }

        return (object) [
            'status' => true,
            'expired_at' => $otp->expiredAt(),
        ];
    }

    /**
     * Create OTP Pin
     * 
     * @return string
     */
    private function createOTPToken(): string
    {
        $isNew = false;
        $token = '';
        while(!$isNew) {
            $token = $this->generateToken();
            $existingOtp = $this->otpUsedAlreadyOrGeneratedInLastDay($token);
            $isNew = empty($existingOtp);
        } 

        return $token;
    }

    /**
     * Generate new OTP token
     * 
     * @return string
     */
    private function generateToken(): string
    {
        $length = strlen(self::OTP_DIGITS);
        $pin = '';
        for ($i = 0; $i < $this->length; $i++) {
            $pin .= self::OTP_DIGITS[rand(0, $length - 1)];
        }

        return $pin;
    }
}