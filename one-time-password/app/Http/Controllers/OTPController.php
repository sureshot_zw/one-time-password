<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OtpRequest;
use App\OTPSettings;
use App\Http\Requests\OtpValidationRequest;
use App\Http\Requests\UpdateOtpSettingsRequest;
use App\Http\Facades\OTP;
use Log;
use DB;
use Exception;

class OTPController extends Controller
{
    /**
     * View page for requesting an OTP
     * 
     * @param Request $request
     * 
     * @return View
     */
    public function viewRequestOtp(Request $request)
    {
        return view('otps.request_otp');
    }

    /**
     * Request OTP
     * 
     * @param OtpREquest $request
     * 
     * @return JsonResponse
     */
    public function requestOtp(OtpRequest $request)
    {
        $otp = OTP::generate($request->getEmail());
        
        return response()->json($otp);
    }

    /**
     * Resend OTP
     * 
     * @param OtpRequest $request
     * 
     * @return JsonResponse
     */
    public function resendOtp(OtpRequest $request)
    {
        $otp = OTP::resendOtp($request->getEmail());
        
        return response()->json($otp);
    }

    /**
     * OTP Validation
     * 
     * @param OtpValidationRequest $request
     * 
     * @return JsonResponse
     */
    public function validateOtp(OtpValidationRequest $request)
    {
        $response = OTP::validate($request->getOtp(), $request->getEmail());
        
        return response()->json($response);
    }

    /**
     * Get OTP settings
     * 
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function getOtpSettings(Request $request)
    {
        return response()->json(OTP::getSettings());
    }

    /**
     * Update OTP settings
     * 
     * @param UpdateOtpSettingsRequest $request
     * 
     * @return JsonResponse
     */
    public function updateOtpSettings(UpdateOtpSettingsRequest $request)
    {
        $data = $request->all();
        foreach(OTPSettings::getOTPSettingKeys() as $key) {
            if(isset($data[$key]) && !empty($data[$key])) {
                try {
                    DB::beginTransaction();
                    $setting = OTPSettings::where('setting', $key)->first();

                    if(!$setting) continue;

                    $setting->value = $data[$key];
                    $setting->save();
                    DB::commit();
                } catch(Exception $ex) {
                    DB::rollBack();
                    Log::emergency($ex->getMessage(), $data);

                    return response()->json([
                        'status' => false,
                        'message' => "An error occured while trying to update OTP settings",
                    ]);
                }
            }
        }

        return response()->json([
            'status' => true,
            'message' => "OTP settings updated",
        ]);
    }

    /**
     * View Validate OTP page
     * 
     * @param Request $request
     * 
     * @return View
     */
    public function viewValidateOtp(Request $request)
    {
        return view('otps.validate_otp');
    }

    /**
     * View OTP settings page
     * 
     * @param Request $request
     * 
     * @return View
     */
    public function viewOtpSettings(Request $request)
    {
        return view('otps.otp_settings');
    }
}
