<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Waavi\Sanitizer\Laravel\SanitizesInput;
use Auth;

class UpdateOtpSettingsRequest extends FormRequest
{
    use SanitizesInput;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     *  Validation rules to be applied to the input.
     *
     *  @return array
     */
    public function rules()
    {
        return [
            'length' => ['required','numeric'],
            'allowed_per_hour' => ['required','numeric'],
            'expiration' => ['required','numeric'],
            'resend_grace_period' => ['required','numeric'],
            'resend_max' => ['required','numeric'],
            'allowed_attempts' => ['required','numeric'],
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     *  @return array
     */
    public function filters()
    {
        return [
            'length' => 'trim|escape|digit',
            'allowed_per_hour' => 'trim|escape|digit',
            'expiration' => 'trim|escape|digit',
            'resend_grace_period' => 'trim|escape|digit',
            'resend_max' => 'trim|escape|digit',
            'allowed_attempts' => 'trim|escape|digit',
        ];
    }
}
