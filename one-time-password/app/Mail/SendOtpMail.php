<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\OTP;

class SendOtpMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * One time password
     * 
     * @var OTP
     */
    protected $otp;

    /**
     * Create a new message instance.
     * 
     * @param Otp $otp
     *
     * @return void
     */
    public function __construct(OTP $otp)
    {
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params = [
            'email' => $this->otp->email,
            'otp' => $this->otp->token,
            'expiration' => $this->otp->validity,
            'generated' => $this->otp
                                ->created_at
                                ->setTimeZone('Africa/Johannesburg')
                                ->toDateTimeString()
        ];

        return $this->from(config('otp-config.mail.from'))
            ->subject('One Time Password')
            ->view('otps.send_otp_html', $params)
            ->text('otps.send_otp_text', $params);
    }
}
