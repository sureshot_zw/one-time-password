<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OTPSettings extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'setting', 'value',
    ];

    /**
     * Tabla name
     * 
     * @var string
     */
    public $table = 'otp_settings';

    public $timestamps = true;

    /**
     * Get OTP Settings Keys
     * 
     * @return array
     */
    public static function getOTPSettingKeys() : array
    {
        return [
            'length', 
            'allowed_per_hour', 
            'expiration', 
            'resend_grace_period', 
            'resend_max', 
            'allowed_attempts'
        ];
    }
}
