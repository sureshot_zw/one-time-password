<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OTPController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/view-request-otp', [OTPController::class, 'viewRequestOtp'])->name('viewRequestOtp');
Route::post('/request-otp', [OTPController::class, 'requestOtp'])->name('requestOtp');
Route::post('/resend-otp', [OTPController::class, 'resendOtp'])->name('resendOtp');
Route::get('/view-validate-otp', [OTPController::class, 'viewValidateOtp'])->name('viewValidateOtp');
Route::post('/validate-otp', [OTPController::class, 'validateOtp'])->name('validateOtp');
Route::get('/view-otp-settings', [OTPController::class, 'viewOtpSettings'])->name('viewOtpSettings');
Route::get('/get-otp-settings', [OTPController::class, 'getOtpSettings'])->name('getOtpSettings');
Route::put('/update-otp-settings', [OTPController::class, 'updateOtpSettings'])->name('updateOtpSettings');